package cz.filek.aos.entity;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Entity for storing all ImageData
 */
public class ImageDataCollection
{
    private ArrayList<ImageData> images;
    private Integer imageSize;
    private BigDecimal maxValue;
    private Integer angle;

    public ImageDataCollection(int angle)
    {
        this.images = new ArrayList<>();
        this.imageSize = null;
        this.angle = angle;
        this.maxValue = new BigDecimal("-1");
    }

    /**
     * Adds new image into the collection
     *
     * @param imageData ImageData
     */
    public void addImageData(ImageData imageData)
    {
        this.images.add(imageData);
        if (this.imageSize == null)
        {
            this.imageSize = imageData.getSize();
        }
        if (BigDecimal.valueOf(imageData.getSumColMax()).compareTo(this.maxValue) > 0)
        {
            this.maxValue = BigDecimal.valueOf(imageData.getSumColMax());
        }
    }

    /**
     * Returns collection of image data
     *
     * @return ArrayList<ImageData>
     */
    public ArrayList<ImageData> getImages()
    {
        return images;
    }

    /**
     * Returns size of collection
     *
     * @return int
     */
    public int getSize()
    {
        return this.images.size();
    }

    /**
     * Returns size of the image
     *
     * @return int
     */
    public int getImageSize()
    {
        return this.imageSize;
    }

    public BigDecimal getMaxValue()
    {
        return maxValue;
    }

    public Integer getAngle()
    {
        return angle;
    }
}
