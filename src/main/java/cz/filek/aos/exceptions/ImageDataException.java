package cz.filek.aos.exceptions;

public class ImageDataException extends Exception
{
    public ImageDataException(String message)
    {
        super(message);
    }
}
