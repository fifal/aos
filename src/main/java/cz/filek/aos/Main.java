package cz.filek.aos;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/main-window.fxml"));
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/ct.jpg")));
        primaryStage.setTitle("CT – Summation, A17N0033P");
        primaryStage.setScene(new Scene(root, 750, 675));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
